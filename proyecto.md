# Tema 1: solución de ecuaciones de una variable y serie de Fourier
Algoritmos a evaluar:
* Bisección
* Secante
* Punto fijo
* Steffensen
* Aitken
* Müller
* Series de Fourier

## Entradas:
### Módulo 1 (solución de ecuaciones de una variable)
* Función $`f(x)`$ o $`g(x)`$, puntos o valores iniciales necesarios para aplicar cada método,
tolerancia $\epsilon$ que se desea cumplir en la aproximación. Lo cual debe
ingresarse en una celda de Jupyter antes de ejecutar el método.

### Módulo 2 (serie de Fourier)
* Función $`f(x)`$ (continua o a trozos), período $`T`$ y grado $`n`$ a utilizar en la expansión
del polinomio trigonométrico de Fourier.
* Punto o puntos en $`x`$ a evaluar en dicho polinomio trigonométrico.

## Salidas:

### Módulo 1 (solución de ecuaciones de una variable)
* Tabla con las iteraciones del método a ejecutar.
* Indicar si sucedió un error y no se logró localizar la raíz.
* Gráfica de la función, en la cual se indicará dónde se ubica la raíz encontrada.

### Módulo 2 (serie de Fourier)
* Valores obtenidos para cada coeficiente de Fourier $`a_0, a_k, b_k`$.
* Serie de Fourier expandida.
* Gráfica del polinomio trigonométrico de Fourier generado.
* Aproximación obtenida al colocar uno o diferentes valores dentro del $`P(x)`$ generado.

# Tema 2: interpolación y aproximación polinomial.
**A evaluar**:
1. Polinomio interpolante de Lagrange.
2. Diferencias divididas de Newton

## Entrada:
* Posibilidad 1: se debe ingresar una nube de puntos y cantidad de valores a interpolar.
* Posibilidad 2: función matemática $`f(x)`$, una serie de valores $`x`$ a evaluar dentro
de la función y los puntos o valores que se desean interpolar.
* **Nota:** se debe de hacer todas las validaciones necesarias para el ingreso de puntos
o nodos.

## Salida:
* Una celda para llamar la interpolación de Newton y otra para utilizar la de Lagrange.
* Una gráfica que contendra:
	* La nube de puntos dada.
	* Punto interpolados.
	* Gráfica de la función $`f(x)`$ ingresada (posibilidad 2)
	* Polinomio generado
	* Gráfica comparativa entre el interpolante y los datos originales.

# Tema 3: mínimos cuadrados
**A evaluar**:
1. Diferentes tipos de regresiones de mínimos cuadrados.
	1. Regresiones polinomiales, exponenciales, potenciales, logarítmicas
2. Casos discretos y continuos.

## Entrada:
* Posibilidad 1 (caso discreto): una nube de puntos (coordenadas en $`x`$ y en $`y`$), en
cualquier orden.
	* En el caso discreto colocar varias celdas con las llamadas a los distintos tipos de interpolación.
	* En el caso continuo colocar una celda que realice la llamada a la aproximación polinomial del grado deseado (hasta grado 4).
* Posibilidad 2 (caso continuo): una función $`f(x)`$ y su respectivo intervalo.
* Una serie de valores que interpolarán (función ajenos a la nube de puntos, solo
para probar).

## Salidas:
* Ecuaciones normales para cada una de las funciones solicitadas por el usuario (matrices
$`\mathbf{A}, \mathbf{c}, \mathbf{b}`$).
* Solución del sistema de ecuaciones (matriz $`\mathbf{c}`$).
* Fórmula de aproximación encontrada.
* Un gráfico por cada llamada a la aproximación:
	* Incluir la nube de puntos (caso discreto)
	* Incluir gráfica de la función $`f(x)`$ (caso continuo)
	* Incluir gráfica con la aproximación encontrada y los valores reales.

# Tema 4: derivación e integración numérica
**A evaluar**:
1. Aproximación de derivadas mediante diferencias centrales.
2. Regla o método de Simpson utilizando 20 particiones.

##Entrada
### Módulo 1
* Integración de función $`f(x)`$, de la forma

```math
I = \int_{a}^{b} f(x) \; \mathrm{d}x
```

* Integración de función $`f(x,y)`$, de la forma

```math
I = \int_{c}^{d} \int_{\alpha(x)}^{\beta(x)} f(x,y) \; \mathrm{d}x \mathrm{d}y
```

* Integración de función $`f(x,y,z)`$, de la forma

```math
I = \int_{a}^{b} \int_{c(x)}^{d(x)} \int_{\alpha (x,y)}^{\beta (x,y)} f(x,y,z) \; \mathrm{d}z \mathrm{d}y \mathrm{d}x
```

### Módulo 2
* Rectas tangentes: se podra digitar una función $`f(x)`$ cualquiera junto con un punto $`x_0`$ y el tamaño de paso $`h`$.

## Salida:
### Módulo 1
Valor de la integral y gráfica de la función en el intervalo de integración.

### Módulo 2
* Gráfica de la función $`f(x)`$ y la recta tangente en el punto $`x_0`$
* Ecuación de la recta tangente a la función en el punto $`x_0`$

# Tema 5: métodos numéricos para resolver ecuaciones diferenciales de primer orden.
**A evaluar**:
1. Método de Euler.
2. Método de Heun.
3. Método de Runge-Kutta de cuarto orden.

## Entrada:
* Ecuación diferenciales de primer orden, de la forma  $`y' = f(x,y)`$.
* Condición inicial $`x_0`$
* Condición inicial $`y_0 = y(x_0)`$
* Valor final de $`x_n`$ del intervalo donde se dejará de iterar.
* Tamaño de paso $`h`$.
* Una celda por método a utilizar.
* En caso de conocer la solución obtenida por métodos analíticos:
	* Función $`f(x)`$ (para contrastar con datos obtenidos mediante el método numérico).

## Salida:
* Tabla con cada una de las iteraciones. En el caso del método de Runge-Kutta agregar
los pasos intermedios $`k_1, k_2, k_3, k_4`$.
* Gráfica de la solución provista por el método numérico (indicar datos/puntos importantes).
* Añadir a cada una de las gráficas anteriores la gráfica de la función $`f(x)`$ obtenida
por métodos analíticos (en caso de haberla ingresado).
* Al final generar una sola gráfica con las tres aproximaciones anteriores.